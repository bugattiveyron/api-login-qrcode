#Methods API (QRCode)
Methods Publics
// <summary>
/// App envia Token a partir da leitura do Qrcode(Token)
/// </summary>
/// <param name="token">Token</param>
/// <returns> IHttpActionResult </returns>
 [HttpGet]
 [Route("auth")]
 public IHttpActionResult AuthQrCode(string token, int idAutorRequest) 

/// <summary>
/// Web Site Solicita Token
/// </summary>
/// <returns> IHttpActionResult </returns>
[HttpGet]
[Route("generatorToken")]
public IHttpActionResult GerarToken()

/// <summary>
/// Método para verificar autenticidade do token
/// </summary>
/// <param name="token"></param>
/// <returns> IHttpActionResult </returns>
 [HttpGet]
 [Route("verifyToken")]
 public IHttpActionResult VerifyAuthToken(string token)
Methods Private
/// <summary>
/// Método gera Token dinâmico, sem precisar atrelar a um usuario
/// </summary>
/// <returns> AdmTokenFila </returns>
private AdmTokenFila GetNewTokenDynamic()


## Browser Compatibility
IE6~10, Chrome, Firefox, Safari, Opera, Mobile Safari, Android, Windows Mobile, ETC.


[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/davidshimjs/qrcodejs/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

